package com.iuranwarga.faces.bean;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.client.RestTemplate;

import com.iuranwarga.model.json.User;
import com.iuranwarga.retrofit.dao.UserDaoImpl;


@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean {
	private String email ;
	private String password;
    public static final String GET_M="http://localhost:8099/user/{email}/{psswd}";
    private RestTemplate restTemplate = new RestTemplate();
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String doLogin() {
//	   	 Map<String, String> stringMap = new HashMap<>();
//	     stringMap.put("email", "david@m.com");
//	     stringMap.put("psswd", "12345");
//	     User user =restTemplate.getForObject(GET_M,User.class,stringMap);
//	     System.out.println(user);
		UserDaoImpl userDaoImpl = new UserDaoImpl();
		User user = userDaoImpl.getUserByEmailPss(email, password);
		System.out.println(user);
        if (user != null) {
//			
//		}
//		if (email.equals("admin") && password.equals("admin")) {
			HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", user.getEmail());
			redirect("/beranda.xhtml");
			return "admin";
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Incorrect Username and Passowrd",
							"Please enter correct username and Password"));
			System.out.println("salah");
			return "login";
		}
	}
	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		redirect("/index.xhtml");
//		FacesContext.getCurrentInstance().getExternalContext()
//		.invalidateSession();
//		FacesContext
//		.getCurrentInstance()
//		.getApplication()
//		.getNavigationHandler()
//		.handleNavigation(FacesContext.getCurrentInstance(), null,
//		"/index.xhtml");
		return "index";
	}
	
	public void redirect(String url) {
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		String xurl = externalContext.encodeActionURL(context.getApplication().getViewHandler().getActionURL(context, url));
		try {
			externalContext.redirect(xurl);
		} catch (IOException e) {
			// TODO: handle exception
			throw new FaceletException(e);
		}
	}
}
