package com.iuranwarga.faces.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.iuranwarga.model.json.Broadcast;
import com.iuranwarga.model.json.User;
import com.iuranwarga.model.json.UserRole;
import com.iuranwarga.retrofit.ApiUtils;
import com.iuranwarga.retrofit.dao.BroadCastDao;
import com.iuranwarga.retrofit.dao.BroadCastDaoImpl;
import com.iuranwarga.retrofit.interfaces.BroadCastService;
import com.iuranwarga.retrofit.response.RetrofitResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Main {
	public static final String GET_M = "http://localhost:8089/user/{email}/{psswd}";
	private RestTemplate restTemplate = new RestTemplate();
	BroadCastService broadCastService;
	
	
	private BroadCastDao broadCastDao;

	private BroadCastDaoImpl broadCastDaoImpl;
	
	@Autowired	
	public BroadCastDaoImpl getBroadCastDaoImpl() {
		return broadCastDaoImpl;
	}

	public void setBroadCastDaoImpl(BroadCastDaoImpl broadCastDaoImpl) {
		this.broadCastDaoImpl = broadCastDaoImpl;
	}

	public static void main(String[] args) {
		Main main = new Main();
		main.t();
	

		Retrofit retrofit = new Retrofit.Builder().baseUrl("http://localhost:8099")
				.addConverterFactory(JacksonConverterFactory.create()).build();

		BroadCastService interfaces = retrofit.create(BroadCastService.class);
		/*
		 * Broadcast broadcast = new Broadcast(); UserRole role = new UserRole();
		 * role.setIdUserRole(1); User user = new User(); user.setId(1);
		 * user.setUserRole(role); broadcast.setInformasi("Maulid Nabi");
		 * broadcast.setLink("user.com"); broadcast.setUser(user);
		 * 
		 * Call<Broadcast> call = interfaces.saveBroadCast(broadcast); call.enqueue(new
		 * Callback<Broadcast>() {
		 * 
		 * @Override public void onResponse(Call<Broadcast> call, Response<Broadcast>
		 * response) { // TODO Auto-generated method stub if (response.isSuccessful()) {
		 * System.out.println("Berhasil di simpan"); }
		 * 
		 * }
		 * 
		 * @Override public void onFailure(Call<Broadcast> call, Throwable t) { // TODO
		 * Auto-generated method stub
		 * 
		 * } });
		 */
//
//		Broadcast broadcast = new Broadcast();
//		UserRole role = new UserRole();
//		role.setIdUserRole(1);
//		User user = new User();
//		user.setId(1);
//		user.setUserRole(role);
//		broadcast.setId(5);
//		broadcast.setInformasi("17 Agustus");
//		broadcast.setLink("user oc.com");
//		broadcast.setUser(user);
//
//		Call<Broadcast> call = interfaces.updateBroadCast(broadcast);
//		call.enqueue(new Callback<Broadcast>() {
//			
//			@Override
//			public void onResponse(Call<Broadcast> call, Response<Broadcast> response) {
//				// TODO Auto-generated method stub
//				if (response.isSuccessful()) {
//					System.out.println("Berhasil di update");
//				}
//				
//			}
//			
//			@Override
//			public void onFailure(Call<Broadcast> call, Throwable t) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
//		Call<Broadcast> call = interfaces.deleteBroadCast("4");
//		call.enqueue(new Callback<Broadcast>() {
//			
//			@Override
//			public void onResponse(Call<Broadcast> call, Response<Broadcast> response) {
//				// TODO Auto-generated method stub
//			System.out.println("berhasil");
//				
//			}
//			
//			@Override
//			public void onFailure(Call<Broadcast> call, Throwable t) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		
//		List<Broadcast> br =  interfaces.get().execute().body();
//			List<Broadcast> br =  br
//			  System.out.println(br);
//			  for (Broadcast broadcast : br) {
//				System.out.println(broadcast.getUser().getNama());
//			}

	}

	public void t() {
//   	 Map<String, String> stringMap = new HashMap<>();
//     stringMap.put("email", "david@gmail.com");
//     stringMap.put("psswd","12345");
//     User user =restTemplate.getForObject(GET_M,User.class,stringMap);
//     System.out.println(user);
//		BroadCastDaoImpl impl = new BroadCastDaoImpl();
//		List<Broadcast> broadcasts = new ArrayList<Broadcast>();
//		broadcasts = broadCastDaoImpl.broadcasts();
//		System.out.println(broadcasts);
//		for (Broadcast broadcast : broadcasts) {
//			System.out.println(broadcast.getUser().getNama());
//		}
		
		broadCastService = ApiUtils.getBroadCastService();
		Call<List<Broadcast>> call = broadCastService.get();
		call.enqueue(new Callback<List<Broadcast>>() {

			@Override
			public void onResponse(Call<List<Broadcast>> call, Response<List<Broadcast>> response) {
				// TODO Auto-generated method stub
				System.out.println();
				List<Broadcast> list = new ArrayList<Broadcast>();
				list = response.body();
				System.out.println(list);
				
			}

			@Override
			public void onFailure(Call<List<Broadcast>> call, Throwable t) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
