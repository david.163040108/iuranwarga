package com.iuranwarga.faces.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.iuranwarga.model.json.Broadcast;
import com.iuranwarga.model.json.User;
import com.iuranwarga.retrofit.ApiClient;
import com.iuranwarga.retrofit.ApiUtils;
import com.iuranwarga.retrofit.dao.BroadCastDao;
import com.iuranwarga.retrofit.dao.BroadCastDaoImpl;
import com.iuranwarga.retrofit.interfaces.BroadCastService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@ManagedBean(name = "broadcastBean")
@SessionScoped
@Component(value = "broadcastBean")
@Scope("session")
public class BroadcastBean {
	private List<Broadcast> broadcasts;
	private Broadcast selectedBroadcast;
	private List<Broadcast> selectedBroadcasts;
	@Autowired
	private BroadCastDao broadCastDao;
	
	private BroadCastService broadCastService;
	
	public void saveBroadcast() {
		
	}
	
	public void deleteBroadCase() {
		
	}
	
	public void update() {
		
	}
	public void openNew() {
		this.selectedBroadcast = new Broadcast();
	}
	
	public boolean hasSelectedBroadcast() {
		return this.selectedBroadcasts != null && !this.selectedBroadcasts.isEmpty();
	}
  
	@PostConstruct
	public void init() {
		broadcasts =new ArrayList<Broadcast>();
//		String GET_M="http://localhost:8099/user/{email}/{psswd}";
//		Broadcast broadcast = new Broadcast();
//		broadcast.setId(1);
//		broadcast.setInformasi("mmm");
//		broadcast.setLink("mmm");
//		broadcasts.add(broadcast);
		RestTemplate restTemplate = new RestTemplate();
//
//		String get = "http://localhost:8099/broadcast/1";
//		ResponseEntity<List<Broadcast>> entity = restTemplate.exchange(get, HttpMethod.GET,null, new ParameterizedTypeReference<List<Broadcast>>() {
//		});
//		System.out.println(entity.getBody());
//		broadcasts=entity.getBody();
//
//	   	 
//		Map<String, String> stringMap = new HashMap<>();
//	     stringMap.put("email", "david@m.com");
//	     stringMap.put("psswd", "12345");
//	     User user = restTemplate.getForObject(GET_M,User.class,stringMap);
//	     System.out.println(user);
//		User user = new User();
//	   	 Map<String, String> stringMap = new HashMap<>();
//	     stringMap.put("email", "david@m.com");
//	     stringMap.put("psswd", "12345");
//	     user =restTemplate.getForObject(GET_M,User.class,stringMap);
//	     System.out.println(user);
	     
//	     Retrofit retrofit = new Retrofit.Builder()
//	    		 .baseUrl("http://localhost:8099")
//	    		 .addConverterFactory(JacksonConverterFactory.create()).build();
	     
//	     BroadCastInterfaces interfaces = ApiClient.getRetrofit().create(BroadCastInterfaces.class);
//	
//		List<Broadcast> br;
//		try {
//			broadcasts = interfaces.get().execute().body();
//			   broadcasts =  br;
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		ini juga berhasil lagi nyari yg cocok buat jsf
//		
//		BroadCastDaoImpl broadCastDaoImpl = new BroadCastDaoImpl();
//		broadcasts = broadCastDaoImpl.broadcasts();
		
		broadCastService = ApiUtils.getBroadCastService();
		Call<List<Broadcast>> call = broadCastService.get();
		call.enqueue(new Callback<List<Broadcast>>() {
			
			@Override
			public void onResponse(Call<List<Broadcast>> call, Response<List<Broadcast>> response) {
				// TODO Auto-generated method stub
				List<Broadcast> b= new ArrayList<Broadcast>();
				b =response.body();
				System.out.println(b);
				broadcasts = b;

			}
			
			@Override
			public void onFailure(Call<List<Broadcast>> call, Throwable t) {
				// TODO Auto-generated method stub
				
			}
		});



	}


	public List<Broadcast> getBroadcasts() {
		return broadcasts;
	}


	public void setBroadcasts(List<Broadcast> broadcasts) {
		this.broadcasts = broadcasts;
	}


	public Broadcast getSelectedBroadcast() {
		return selectedBroadcast;
	}


	public void setSelectedBroadcast(Broadcast selectedBroadcast) {
		this.selectedBroadcast = selectedBroadcast;
	}


	public List<Broadcast> getSelectedBroadcasts() {
		return selectedBroadcasts;
	}


	public void setSelectedBroadcasts(List<Broadcast> selectedBroadcasts) {
		this.selectedBroadcasts = selectedBroadcasts;
	}


	public BroadCastDao getBroadCastDao() {
		return broadCastDao;
	}


	public void setBroadCastDao(BroadCastDao broadCastDao) {
		this.broadCastDao = broadCastDao;
	}

}
