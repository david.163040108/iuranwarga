package com.iuranwarga.spring.security;

import com.iuranwarga.model.json.DetailUser;
import com.iuranwarga.model.json.User;

public class UserSession {

	private User user;
	private DetailUser detailUser;
	
	
	public UserSession(User user, DetailUser detailUser) {
		super();
		this.user = user;
		this.detailUser = detailUser;
	}
	public UserSession() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public DetailUser getDetailUser() {
		return detailUser;
	}
	public void setDetailUser(DetailUser detailUser) {
		this.detailUser = detailUser;
	}
	
}
