package com.iuranwarga.spring.security;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

public class CustomAuthenticationHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// TODO Auto-generated method stub
		
		System.out.println("---request---");
		System.out.println(""+request.getUserPrincipal());
		
		System.out.println("---authentication---");
		System.out.println("detail        : "+authentication.getDetails()); 
		System.out.println("name          : "+authentication.getName());
		System.out.println("Authorities   : "+authentication.getAuthorities());
		System.out.println("Credential    : "+authentication.getCredentials());
		System.out.println("Principal     : "+authentication.getPrincipal());
		
		
		String adminTargetUrl = "/faces/admin/index.xhtml";
		String rtTargetUrl = "/faces/rt/index.xhtml";
		String wargaTargetUrl = "/faces/warga/index.xhtml";
		Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());	
		
		if (roles.contains("ROLE_ADMINISTRATOR")) {
			getRedirectStrategy().sendRedirect(request, response, adminTargetUrl);
		} else if (roles.contains("ROLE_ADMIN_FAKULTAS")) {
			getRedirectStrategy().sendRedirect(request, response, rtTargetUrl);
		} else if (roles.contains("ROLE_MAHASISWA")) {
			getRedirectStrategy().sendRedirect(request, response, wargaTargetUrl);
		} 
		else {
			super.onAuthenticationSuccess(request, response, authentication);

		}
	}

}
