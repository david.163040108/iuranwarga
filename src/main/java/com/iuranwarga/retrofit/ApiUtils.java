package com.iuranwarga.retrofit;

import com.iuranwarga.retrofit.interfaces.BroadCastService;
import com.iuranwarga.retrofit.interfaces.UserService;

public class ApiUtils {
	private ApiUtils() {

	}
	
	public static BroadCastService getBroadCastService() {
		return ApiClient.getRetrofit().create(BroadCastService.class);
	}
	
	public static UserService getUserService() {
		return ApiClient.getRetrofit().create(UserService.class);
	}
	
}
