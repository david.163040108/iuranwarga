package com.iuranwarga.retrofit.interfaces;

import java.util.List;

import com.iuranwarga.model.json.Broadcast;
import com.iuranwarga.retrofit.response.RetrofitResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BroadCastService {
	@GET("/broadcast/1")
	Call<List<Broadcast>> get();
	
	@POST("/broadcast/save")
	Call<Broadcast> saveBroadCast(@Body Broadcast broadcast);
	
	@PUT("/broadcast/update")
	Call<Broadcast> updateBroadCast(@Body Broadcast broadcast);
	@DELETE("/broadcast/delete/{id}")
	Call<Broadcast> deleteBroadCast(@Path("id") String id);
}
