package com.iuranwarga.retrofit.interfaces;

import com.iuranwarga.model.json.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UserService {
	@GET("/user/{email}/{password}")
	Call<User> getUserByEmail(@Path("email") String user, @Path("password") String pss);
}
