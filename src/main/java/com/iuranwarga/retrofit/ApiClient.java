package com.iuranwarga.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ApiClient {
	private static Retrofit retrofit = null;
	
	public static Retrofit getRetrofit() {
		if (retrofit == null) {
			retrofit = new Retrofit.Builder().baseUrl("http://localhost:8099")
					.addConverterFactory(JacksonConverterFactory.create()).build();
		}
		
		return retrofit;
	}
}
