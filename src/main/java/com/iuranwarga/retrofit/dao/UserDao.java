package com.iuranwarga.retrofit.dao;

import com.iuranwarga.model.json.User;

public interface UserDao {
	public User getUserByEmailPss(String email, String pss);
}
