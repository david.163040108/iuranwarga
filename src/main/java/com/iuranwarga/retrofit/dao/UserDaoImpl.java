package com.iuranwarga.retrofit.dao;

import java.io.IOException;

import com.iuranwarga.model.json.User;
import com.iuranwarga.retrofit.ApiClient;
import com.iuranwarga.retrofit.interfaces.UserService;

public class UserDaoImpl implements UserDao {

	@Override
	public User getUserByEmailPss(String email, String pss) {
		UserService service = ApiClient.getRetrofit().create(UserService.class);
		User user= null;
			try {
				user = new User();
				user = service.getUserByEmail(email, pss).execute().body();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return user;
	}

}
