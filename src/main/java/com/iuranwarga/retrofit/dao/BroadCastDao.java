package com.iuranwarga.retrofit.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.iuranwarga.model.json.Broadcast;

public interface BroadCastDao {
	public List<Broadcast> broadcasts();
}
