package com.iuranwarga.retrofit.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.iuranwarga.model.json.Broadcast;
import com.iuranwarga.retrofit.ApiClient;
import com.iuranwarga.retrofit.interfaces.BroadCastService;
@Repository("broadCastDao")
@Transactional
public class BroadCastDaoImpl implements BroadCastDao {

	@Override
	public List<Broadcast> broadcasts() {
		BroadCastService broadCastInterfaces = ApiClient.getRetrofit().create(BroadCastService.class);
		List<Broadcast> list = null;
		try {
			list = new ArrayList<Broadcast>();
			list = broadCastInterfaces.get().execute().body();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
