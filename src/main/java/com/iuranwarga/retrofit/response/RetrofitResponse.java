package com.iuranwarga.retrofit.response;

public class RetrofitResponse {
	private Boolean status;

	public RetrofitResponse(Boolean status) {
		super();
		this.status = status;
	}

	public RetrofitResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
}
