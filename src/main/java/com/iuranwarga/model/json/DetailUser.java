package com.iuranwarga.model.json;

import javax.persistence.*;

public class DetailUser {

    private Integer id;

    private String nik;
    private String noRumah;

    private String jmlAnggotaKeluarga;

    private String noHp;

    private User user;

    private String idRt;

	public DetailUser(Integer id, String nik, String noRumah, String jmlAnggotaKeluarga, String noHp, User user,
			String idRt) {
		super();
		this.id = id;
		this.nik = nik;
		this.noRumah = noRumah;
		this.jmlAnggotaKeluarga = jmlAnggotaKeluarga;
		this.noHp = noHp;
		this.user = user;
		this.idRt = idRt;
	}

	public DetailUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNoRumah() {
		return noRumah;
	}

	public void setNoRumah(String noRumah) {
		this.noRumah = noRumah;
	}

	public String getJmlAnggotaKeluarga() {
		return jmlAnggotaKeluarga;
	}

	public void setJmlAnggotaKeluarga(String jmlAnggotaKeluarga) {
		this.jmlAnggotaKeluarga = jmlAnggotaKeluarga;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIdRt() {
		return idRt;
	}

	public void setIdRt(String idRt) {
		this.idRt = idRt;
	}

	@Override
	public String toString() {
		return "DetailUser [id=" + id + ", nik=" + nik + ", noRumah=" + noRumah + ", jmlAnggotaKeluarga="
				+ jmlAnggotaKeluarga + ", noHp=" + noHp + ", user=" + user + ", idRt=" + idRt + "]";
	}
    
    
}
