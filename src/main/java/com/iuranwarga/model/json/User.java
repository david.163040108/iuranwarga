package com.iuranwarga.model.json;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

public class User {
    private Integer id;
    private String nama;
    private String email;
    private String password;
    private UserRole userRole;
    
	public User(Integer id, String nama, String email, String password, UserRole userRole) {
		super();
		this.id = id;
		this.nama = nama;
		this.email = email;
		this.password = password;
		this.userRole = userRole;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", nama=" + nama + ", email=" + email + ", password=" + password + ", userRole="
				+ userRole + "]";
	}

    
}
