package com.iuranwarga.model.json;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

public class UserRole {
    private Integer idUserRole;
    private String namaRole;
    
    
    
	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserRole(Integer idUserRole, String namaRole) {
		super();
		this.idUserRole = idUserRole;
		this.namaRole = namaRole;
	}
	public Integer getIdUserRole() {
		return idUserRole;
	}
	public void setIdUserRole(Integer idUserRole) {
		this.idUserRole = idUserRole;
	}
	public String getNamaRole() {
		return namaRole;
	}
	public void setNamaRole(String namaRole) {
		this.namaRole = namaRole;
	}
	@Override
	public String toString() {
		return "UserRole [idUserRole=" + idUserRole + ", namaRole=" + namaRole + "]";
	}
    
}
