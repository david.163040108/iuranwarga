package com.iuranwarga.model.json;



import javax.persistence.*;


public class Broadcast {

	    private Integer id;

	    private User user;

	    private String informasi;

	    private String link;

		public Broadcast() {
			super();
			// TODO Auto-generated constructor stub
		}

		public Broadcast(Integer id, User user, String informasi, String link) {
			super();
			this.id = id;
			this.user = user;
			this.informasi = informasi;
			this.link = link;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public String getInformasi() {
			return informasi;
		}

		public void setInformasi(String informasi) {
			this.informasi = informasi;
		}

		public String getLink() {
			return link;
		}

		public void setLink(String link) {
			this.link = link;
		}

		@Override
		public String toString() {
			return "Broadcast [id=" + id + ", user=" + user + ", informasi=" + informasi + ", link=" + link + "]";
		}
	    
	    
}
